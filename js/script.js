function positions(){
  var leftreport = $('#reportes').offset();
  leftreport = leftreport.left;
  $('.reportes-parent').css('left',leftreport);
}
function cargarSeccion(html){
  var clase = html+"-parent";
  html = 'htmls/'+html+'.html';
  var div= "<div class='"+ clase +"'></div>";
  $(div).appendTo('.big-cont');
  $('.'+clase).load(html, function(){
    seguimientosize();
    deviceSize();
  });
}
function cargarReporte(html){
  html = 'htmls/reportes/'+html+'.html';
  var div= "<div class='block-parent'><div class='reporte-parent'></div></div>";
  $(div).appendTo('.big-cont');
  $('.reporte-parent').load(html);
}
function deviceSize(){
  var seguimiento = $('.seguimiento-parent').width() + 30;
  var ancho = $(window).width() - seguimiento;
  $('.device-parent').width(ancho);
}
function seguimientosize(){
  var altura = $(window).height() - 70;
  var scroll = altura - 100;
  $('.seguimiento-parent').height(altura);
  $('.devices-cont').height(scroll);
}
function removeSeccion(html){
  var clase = html+"-parent";
  $('.'+clase).remove();
}
$(document).ready(function(){
  var html = 'seguimiento';
  cargarSeccion(html);
});
$(document).on('click', '.nav-top li', function(){
  var html = $(this).attr('id');
  if ($(this).hasClass('select-menu')) {
    $(this).removeClass('select-menu');
    if (html=='seguimiento') {
      $('.device-parent').remove();
    }
    removeSeccion(html);
  }else{
    $(this).addClass('select-menu');
    cargarSeccion(html);
    if ($(this).hasClass('drop-li')) {
      var left = $(this).offset();
      left = left.left;
      $('.'+html+'-parent').css('left', left);
    }
  }

});
$( window ).resize(function() {
    seguimientosize();
    deviceSize();
    positions();
});
$(document).on('click', '.device-row', function(){
   var num = $('.device-parent').length;
   $('.device-parent').remove();
   if (num == 0) {
     var html = 'device';
     cargarSeccion(html);
   }
});
$(document).on('click', '.indiv-report', function(){
  var html = $(this).attr('id');
  $('.block-parent').remove();
  cargarReporte(html);
  $('.reportes-parent').remove();
  $('#reportes').removeClass('select-menu');
});
$(document).on('click', '.close-pop button', function(){
  $('.block-parent').remove();
});
